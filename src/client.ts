import axios from 'axios'

export const client = axios.create({
    baseURL: 'http://api.hotsdraft.jasonpoindexter.io',
    timeout: 1000,
})
